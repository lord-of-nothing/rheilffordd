-- топ самых длинных перегонов по филиалам РЖД
select dense_rank() over (w) as rank, s1.subsidiary_id, s1.name, s2.name, length
from stage
         join station s1 on stage.station1_id = s1.id
         join station s2 on station2_id = s2.id
window w as ( partition by s1.subsidiary_id
        order by length desc )
order by rank;
