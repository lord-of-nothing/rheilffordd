-- для примера next_trains
insert into schedule values (default, 9, false, 8, '2024-04-23 10:50', '2024-04-23 10:52');

-- возвращает информацию о ближайшем поезде, отправляющемся от указанной станции
create or replace function next_trains(station text, cur_time timestamp default now())
    returns table
            (
                start     text,
                finish    text,
                departure timestamp
            )
as
'select start, finish, min(departure) as next_train
 from schedule_human_readable
 where departure > $2
   and start = $1
 group by start, finish' language sql;

-- возвращает число вагонов некоторого типа в некотором депо
create or replace function cart_count(type text, yard text default '%') returns int as
'select count(*)
 from cart_full
 where model_name = $1
   and yard_code like yard
 group by model_name' language sql;

select *
from cart_count('ЭД4М', 'ТЧПРИГ-14');

-- возвращает число вагонов в составе
create or replace function train_length(name text) returns int as
'select count(*)
 from cart_full
 where train_name = $1
 group by train_name' language sql;

select *
from train_length('7223');

-- триггер -- при помещении вагона в ремонтное депо отвязывает его от состава
create function block_cart() returns trigger as
$$
begin
    update cart
    set train_id = null
    where id = new.id
      and id in (select id from cart_full where yard_type = 'ремонтное');
    return null;
end;
$$
    language plpgsql;
create trigger maintenance_cart
    after insert or update of yard_id
    on cart
    for each row
execute function block_cart();