-- число вагонов каждого типа
select train_model.name, count(*) as amount
from cart
         join train on cart.train_id = train.id
         join train_model on train.model_id = train_model.id
group by train_model.name;

