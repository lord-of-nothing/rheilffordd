-- дата производства первого вагона данного типа
select cart.code, build_date, min(build_date) over (partition by train_model.name)
from cart
         join train on cart.train_id = train.id
         join train_model on train.model_id = train_model.id;
