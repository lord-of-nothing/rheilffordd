-- чтобы некоторые действия дальше работали не с пустотой
insert into yard values (17, 'ТЧР-15', 'Лихоборы', 'ремонтное', 3);
insert into cart values (default, 'КДЭ161-170', null, '', 17, '1967-01-01');

-- расписание с номерами поездов и названиями станций
create or replace view schedule_human_readable as
with tmp as (select schedule.*,
                    case when schedule.direction = true then stage.station2_id else station1_id end as finish_id,
                    case when schedule.direction = true then stage.station1_id else station2_id end as start_id
             from schedule
                      join stage on schedule.stage_id = stage.id)
select train.name as train,
       ss.name    as start,
       sf.name    as finish,
       start_time as departure,
       end_time   as arrival
from tmp
         join station sf on tmp.finish_id = sf.id
         join station ss on tmp.start_id = ss.id
         join train on tmp.train_id = train.id;

-- полная информация о вагоне и поезде, к которому они принадлежат, в прошлой части проекта понадобилось неоднократно
create or replace view cart_full as
select cart.id          as id,
       cart.code        as cart_code,
       cart.type        as cart_type,
       cart.build_date  as cart_build_date,
       t.name           as train_name,
       train_model.name as model_name,
       factory,
       role,
       y.code           as yard_code,
       y.name           as yard_name,
       y.type           as yard_type
from cart
         left join train t on cart.train_id = t.id
         left join train_model on t.model_id = train_model.id
         left join yard y on cart.yard_id = y.id;

-- станции в Москве и Московской области (правда, на данный момент других здесь нет)
create or replace view station_moscow as
select *
from station
where region in (45, 46);

-- вагоны в ремонте -- они же вагоны, прикреплённые к ремонтным депо
-- этот view основан на другом view
create or replace view repairing_carts as
select cart_code, cart_type, cart_build_date, model_name, yard_code, yard_name
from cart_full
where yard_type = 'ремонтное';