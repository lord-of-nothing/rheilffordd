-- все депо, к которым (по мнению этой таблицы) приписано хотя бы 15 вагонов (число произвольное)
select yard.code as code
from cart
         join yard on cart.yard_id = yard.id
group by yard.id
having count(*) >= 15;
