-- все станции МЖД с островными платформами
select name
from station
where station.subsidiary_id = (select id from subsidiary where name = 'Московская')
  and station.platform_type = 'островные'
order by name;

