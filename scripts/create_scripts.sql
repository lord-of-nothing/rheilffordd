create schema railways;

create table "stage"
(                                   -- перегоны
    "id"          serial primary key,
    "station1_id" integer not null, -- id одного конца перегона
    "station2_id" integer not null, -- id другого конца перегона
    "length"      real,             -- длина перегона (в километрах, потому что в метрах не гуглится)
    "electricity" text              -- данные об электрификации
);

create table "station"
(                                   -- станции
    "id"             serial primary key,
    "name"           text not null, -- имя станции
    "region"         integer,       -- код региона (ОКАТО)
    "subsidiary_id"  integer,       -- id филиала ржд, к которому относится станция
    "opening_year"   integer,       -- год открытия
    "platform_count" integer,       -- число платформа
    "platform_type"  text           -- тип платформ
);

create table "subsidiary"
(                          -- филиалы ржд
    "id"      serial primary key,
    "name"    text unique, -- имя филиала
    "hq_city" text         -- город, где находится штаб-квартира
);

create table "yard"
(                           -- депо
    "id"            serial primary key,
    "code"          text,   -- обозначение депо
    "name"          text,   -- название депо
    "type"          text,   -- тип депо (локомотивное, моторвагонное...)
    "subsidiary_id" integer -- id филиала
);

create table "cart"
(                                      -- вагоны
    "id"         serial primary key,
    "code"       text unique not null, -- номер, присвоенный вагону (например, эп2д-000804)
    "train_id"   integer,              -- id состава, к которому относится вагон
    "type"       text,                 -- тип вагона (например, моторный или прицепной в электропоезде)
    "yard_id"    integer,              -- id депо
    "build_date" date                  -- дата выпуска (месяц и год)
);

create table "train_model"
(                          -- виды поездов
    "id"      serial primary key,
    "name"    text unique, -- название модели (например, эп2д)
    "factory" text,        -- завод-производитель
    "role"    text         -- тип (пассажирский/грузовой...)
);

create table "train"
(                            -- составы
    "id"            serial primary key,
    "name"          text,    -- имя поезда (как в расписании)
    "subsidiary_id" integer, -- имя филиала (если внутри одного)
    "model_id"      integer, -- модель поезда
    unique(name, subsidiary_id)
);

create table "schedule"
(                                    -- расписание
    "id"         serial primary key,
    "stage_id"   integer   not null, -- id перегона
    "direction"  boolean   not null, -- true, если от первой станции ко второй, иначе false
    "train_id"   integer   not null, -- id поезда
    "start_time" timestamp not null, -- время отправления с первой станции
    "end_time"   timestamp not null  -- время прибытия на вторую станцию
);



alter table stage
    add constraint fk_stage_station1 foreign key (station1_id) references station (id);

alter table stage
    add constraint fk_stage_station2 foreign key (station2_id) references station (id);

alter table station
    add constraint fk_station_subsidiary foreign key (subsidiary_id) references subsidiary (id);

alter table yard
    add constraint fk_yard_subsidiary foreign key (subsidiary_id) references subsidiary (id);

alter table cart
    add constraint fk_cart_yard foreign key (yard_id) references yard (id);

alter table cart
    add constraint fk_cart_train foreign key (train_id) references train (id);

alter table train
    add constraint fk_train_model foreign key (model_id) references train_model (id);

alter table train
    add constraint fk_train_subsidiary foreign key (subsidiary_id) references subsidiary (id);

alter table schedule
    add constraint fk_schedule_train foreign key (train_id) references train (id);

alter table schedule
    add constraint fk_schedule_stage foreign key (stage_id) references stage (id);