-- интервал между отправлением двух последовательных поездов в одном направлении со станции
with tmp as (select schedule.*,
                    case when schedule.direction = true then stage.station2_id else station1_id end as finish_id,
                    case when schedule.direction = true then stage.station1_id else station2_id end as start_id
             from schedule
                      join stage on schedule.stage_id = stage.id)
select ss.name                                                                                               as start,
       sf.name                                                                                               as finish,
       start_time,
       start_time - lag(start_time) over (partition by (start_id, finish_id, direction) order by start_time) as interval
from tmp
         join station sf on tmp.finish_id = sf.id
         join station ss on tmp.start_id = ss.id;
