-- количество вагонов каждого типа среди произведённых ДМЗ
select train_model.name, count(*)
from cart
         join train on cart.train_id = train.id
         join train_model on train.model_id = train_model.id
where train_model.factory = 'ДМЗ'
group by (train_model.name);
