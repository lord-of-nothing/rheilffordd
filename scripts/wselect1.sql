-- для каждой строки расписания показываем ближайший поезд, прибывающий на ту же станцию в том же направлении (от 1 января 2024)
with tmp as (select schedule.*,
                    case when schedule.direction = true then stage.station2_id else station1_id end as finish_id,
                    case when schedule.direction = true then stage.station1_id else station2_id end as start_id
             from schedule
                      join stage on schedule.stage_id = stage.id
             where end_time >= '2024-01-01 00:00')
select ss.name                                                                as start,
       sf.name                                                                as finish,
       tmp.end_time                                                           as arrival,
       min(tmp.end_time) over (partition by (start_id, finish_id, direction)) as earliest_arrival
from tmp
         join station sf on tmp.finish_id = sf.id
         join station ss on tmp.start_id = ss.id;

