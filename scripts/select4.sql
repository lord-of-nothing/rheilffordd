-- поезда, самый старший вагон которых произведён не раньше 2018 года
select train.name, min(build_date) as oldest_cart
from cart
         join train on cart.train_id = train.id
group by (train.name)
having min(build_date) >= '2018-01-01 00:00';
