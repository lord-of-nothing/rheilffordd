-- разность длины текущего перегона и средней длины в филиале
select ss.name                                                                        as start,
       sf.name                                                                        as finish,
       length,
       round((avg(length) over (partition by sf.subsidiary_id))::numeric, 3)          as avg_length,
       round((length - avg(length) over (partition by sf.subsidiary_id))::numeric, 3) as delta_len
from stage
         join station sf on stage.station2_id = sf.id
         join station ss on stage.station1_id = ss.id;
