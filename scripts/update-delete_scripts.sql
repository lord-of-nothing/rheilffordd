-- на Википедии указаны платформы обоих типов, можно поменять
update station
set platform_type = 'боковые'
where name = 'Подольск';

-- состав был достроен полностью в 2020 году, в 2019 -- только некоторое неизвестное число его вагонов
update cart
set build_date = '2020-11-01'
where code like 'ЭГ2Тв-027%';

-- развернём поезд
update schedule
set direction = not direction
where train_id = 3;

-- приписывать к таким депо ничего не планирую, можно и убрать
delete
from yard
where type = 'ремонтное';

-- базу про метро делаю не я
delete
from train_model
where factory = 'Метровагонмаш';

